import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/views/screen/sign-in_1.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Abel',
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      getPages: AppRoutes.routes,
      home: const Sign_in_1(),
    );
  }
}
