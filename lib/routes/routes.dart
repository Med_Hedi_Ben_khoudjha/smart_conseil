import 'package:get/get.dart';
import 'package:smart_conseil/logic/bindings/main-binding.dart';
import 'package:smart_conseil/views/screen/add-child.dart';
import 'package:smart_conseil/views/screen/add-device.dart';
import 'package:smart_conseil/views/screen/add-profile.dart';
import 'package:smart_conseil/views/screen/bienvenu.dart';
import 'package:smart_conseil/views/screen/child-dashboard.dart';
import 'package:smart_conseil/views/screen/child-dashboard2.dart';
import 'package:smart_conseil/views/screen/main_screen.dart.dart';
import 'package:smart_conseil/views/screen/confirm-profile.dart';
import 'package:smart_conseil/views/screen/congrat-wizard-end.dart';
import 'package:smart_conseil/views/screen/congrats-wizard-end2.dart';
import 'package:smart_conseil/views/screen/sign-in_1.dart';
import 'package:smart_conseil/views/screen/sign-in_2.dart';
import 'package:smart_conseil/views/screen/sign-in_3.dart';
import 'package:smart_conseil/views/screen/space-chose.dart';

class AppRoutes {
  static final routes = [
    GetPage(name: Routes.sign_in_1, page: () => const Sign_in_1()),
    GetPage(name: Routes.sign_in_2, page: () => const Sign_in_2()),
    GetPage(name: Routes.sign_in_3, page: () => const Sign_in_3()),
    GetPage(name: Routes.space_chose, page: () => const SpaceChose()),
    GetPage(name: Routes.bienvenu, page: () => const Bienvenu()),
    GetPage(name: Routes.addDevice, page: () => const AddDevice()),
    GetPage(name: Routes.addProfile, page: () => const AddProfile()),
    GetPage(name: Routes.confirmProfile, page: () => const ConfirmProfile()),
    GetPage(name: Routes.addChild, page: () => const AddChild()),
    GetPage(
        name: Routes.congratsWizardEnd, page: () => const CongratsWizardEnd()),
    GetPage(
        name: Routes.congratsWizardEnd2,
        page: () => const CongratsWizardEnd2()),
    GetPage(
        bindings: [MainBinding()],
        name: Routes.mainScreen,
        page: () => MainScreen()),
    GetPage(
        name: Routes.childDashboard,
        page: () => ChildDashboard()),
         GetPage(name: Routes.childDashboard2, page: () => ChildDashboard2()),
  ];
}

class Routes {
  // ignore: constant_identifier_names
  static const sign_in_1 = '/sign_in_1';
  // ignore: constant_identifier_names
  static const sign_in_2 = '/sign_in_2';
  // ignore: constant_identifier_names
  static const sign_in_3 = '/sign_in_3';
  // ignore: constant_identifier_names
  static const space_chose = '/space_chose';
  // ignore: constant_identifier_names
  static const bienvenu = '/bienvenu';
  // ignore: constant_identifier_names
  static const addDevice = '/addDevice';
  // ignore: constant_identifier_names
  static const addProfile = '/addProfile';
  // ignore: constant_identifier_names
  static const confirmProfile = '/confirmProfile';
  // ignore: constant_identifier_names
  static const addChild = '/addChild';
  // ignore: constant_identifier_names
  static const congratsWizardEnd = '/congratsWizardEnd';
  // ignore: constant_identifier_names
  static const congratsWizardEnd2 = '/congratsWizardEnd2';
  // ignore: constant_identifier_names
  static const mainScreen = '/mainScreen';
   // ignore: constant_identifier_names
  static const childDashboard = '/childDashboard';
     // ignore: constant_identifier_names
  static const childDashboard2 = '/childDashboard2';
}
