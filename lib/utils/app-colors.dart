import 'package:flutter/animation.dart';

class AppColors{

  static Color primary = Color(0xFF3F63A9);
  static Color secondary = Color(0xFF5D4AFF);
}