import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/sign-in-2/button-widget.dart';

class ConfirmProfile extends StatelessWidget {
  const ConfirmProfile({super.key});

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: AppColors.primary,
        body: SafeArea(
            child: Container(
          height: _deviceHeight,
          width: _devicewidth,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 40, bottom: 20),
                width: _devicewidth * 0.8,
                child: Text(
                  "Your child’s profile information has been successfully added ",
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 20),
                height: _deviceHeight * 0.15,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.contain,
                        image: AssetImage(AppAssets.vector))),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                height: _deviceHeight * 0.3,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.contain,
                        image: AssetImage(AppAssets.group2))),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0, right: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: BottonWidget(
                        height: _deviceHeight,
                        text: "Add new profile",
                        onPressed: () {
                          Get.offNamed(Routes.addProfile);
                        },
                        width: _devicewidth * 0.45,
                        haveIcon: false,
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Expanded(
                      child: BottonWidget(
                        height: _deviceHeight,
                        text: "Continue",
                        onPressed: () {
                          Get.offNamed(Routes.addChild);
                        },
                        width: _devicewidth * 0.45,
                        haveIcon: true,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              BottonWidget(
                height: _deviceHeight,
                text: "Modify profile information",
                onPressed: () {
                  Get.offNamed(Routes.confirmProfile);
                },
                width: _devicewidth * 0.7,
                haveIcon: false,
              ),
              SizedBox(
                height: 15,
              ),
            ],
          ),
        )));
  }
}
