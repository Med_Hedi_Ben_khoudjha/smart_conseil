import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/sign-in-2/button-widget.dart';

class CongratsWizardEnd2 extends StatelessWidget {
  const CongratsWizardEnd2({super.key});

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: AppColors.primary,
        body: SafeArea(
            child: Container(
          height: _deviceHeight,
          width: _devicewidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top: 40, bottom: 60),
                width: _devicewidth * 0.8,
                child: Text(
                  "Thank you for your confidence in us.",
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 50),
                height: _deviceHeight * 0.3,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.contain,
                        image: AssetImage(AppAssets.logo2))),
              ),
              SizedBox(
                height: 15,
              ),
              BottonWidget(
                height: _deviceHeight,
                text: "Go to dashboard",
                onPressed: () {
                  Get.offNamed(Routes.mainScreen);
                },
                width: _devicewidth * 0.5,
                haveIcon: true,
              ),
            ],
          ),
        )));
  }
}
