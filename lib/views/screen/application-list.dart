import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/application-list/search-form-text.dart';

class ApplicationList extends StatefulWidget {
  const ApplicationList({super.key});

  @override
  State<ApplicationList> createState() => _ApplicationListState();
}

class _ApplicationListState extends State<ApplicationList> {
  bool isSwitched = false;
  bool isSwitched2 = true;

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: AppColors.primary,
        appBar: AppBar(
          elevation: 0,
          title: Text(
            "Parental Control",
            style: TextStyle(fontSize: 25),
          ),
          backgroundColor: AppColors.primary,
          centerTitle: true,
          leading: Icon(
            Icons.menu,
            size: 35,
          ),
          actions: [
            Icon(
              Icons.person,
              size: 35,
            )
          ],
        ),
        body: SafeArea(
            child: Container(
          height: _deviceHeight,
          width: _devicewidth,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SearchFormText(),
              ),
              new Divider(
                color: Colors.white,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Image(height: 30, image: AssetImage(AppAssets.google)),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Google Chrome",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Switch(
                          value: isSwitched,
                          onChanged: (value) {
                            setState(() {
                              isSwitched = value;
                              print(isSwitched);
                            });
                          },
                          activeTrackColor: Colors.lightGreenAccent,
                          activeColor: Colors.green,
                        ),
                      ],
                    ),
                  )
                ],
              ),
              new Divider(
                color: Colors.grey,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Image(height: 30, image: AssetImage(AppAssets.camera)),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Camera",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Switch(
                          value: isSwitched2,
                          onChanged: (value) {
                            setState(() {
                              isSwitched2 = value;
                              print(isSwitched2);
                            });
                          },
                          activeTrackColor: Colors.lightGreenAccent,
                          activeColor: Colors.green,
                        ),
                      ],
                    ),
                  )
                ],
              ),
              new Divider(
                color: Colors.grey,
              ),
            ],
          ),
        )));
  }
}
