import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/sign-in-2/button-widget.dart';

class Bienvenu extends StatelessWidget {
  const Bienvenu({super.key});

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: AppColors.primary,
        body: SafeArea(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 60),
              width: _devicewidth * 0.8,
              child: Text(
                "Welcom to 4IN Shield",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 60),
              width: double.infinity,
              height: _deviceHeight * 0.4,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.contain, image: AssetImage(AppAssets.chatting))),
            ),
            BottonWidget(
              height: _deviceHeight,
              text: "Start",
              width: _devicewidth * 0.3,
              onPressed: () {
                Get.offNamed(Routes.addDevice);
              },
              haveIcon: true,
            ),
          ],
        )));
  }
}
