import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/child-dashboard/child-dashboard-widget.dart';
import 'package:smart_conseil/views/widget/child-dashboard/list-widget.dart';

class ChildDashboard extends StatefulWidget {
  ChildDashboard({super.key});

  @override
  State<ChildDashboard> createState() => _ChildDashboardState();
}

class _ChildDashboardState extends State<ChildDashboard> {
  int _selected = 0;

  @override
  Widget build(BuildContext context) {
    final double _deviceHeight = MediaQuery.of(context).size.height;
    final double _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AppColors.primary,
      appBar: AppBar(
        elevation: 0,
        title: Text(
          "Child first name",
          style: TextStyle(fontSize: 25),
        ),
        backgroundColor: AppColors.primary,
        centerTitle: true,
        leading: Icon(
          Icons.menu,
          size: 35,
        ),
        actions: [
          Icon(
            Icons.person,
            size: 35,
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          height: _deviceHeight,
          width: _devicewidth,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: _deviceHeight * 0.15,
                  width: _devicewidth * 0.3,
                  margin: EdgeInsets.only(top: 30, left: 40),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white),
                  child: Image.asset(AppAssets.girl12, fit: BoxFit.contain),
                ),
                Container(
                  margin: EdgeInsets.only(top: 30, left: 40),
                  child: Text(
                    "First name Last name",
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.ltr,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 3, left: 40),
                  child: Text(
                    "AGE",
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.ltr,
                    style: TextStyle(color: Colors.white24, fontSize: 20),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10, left: _devicewidth / 5),
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _icon(
                        0,
                        text: "Month",
                      ),
                      _icon(
                        1,
                        text: "Week",
                      ),
                      _icon(
                        2,
                        text: "Day",
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ChildDashboardWidget(
                  deviceHeight: _deviceHeight,
                  devicewidth: _devicewidth,
                  title: "Danger Level",
                  title2: "Last night Sleep duration",
                  description: "Height",
                  description2: "8h",
                  haveColor: true,
                ),
                ChildDashboardWidget(
                    deviceHeight: _deviceHeight,
                    devicewidth: _devicewidth,
                    title: "Device usage",
                    description: "4H",
                    description2: "4h",
                    haveColor: false,
                    title2: "Phone call duration"),
                Listwidget(
                    deviceHeight: _deviceHeight, devicewidth: _devicewidth),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _icon(int index, {String? text}) {
    return InkResponse(
      child: Container(
        width: 80,
        height: 40,
        margin: EdgeInsets.only(top: 3, left: 3),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: _selected == index ? Colors.white : AppColors.primary),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(text!,
                style: TextStyle(
                    fontSize: 20,
                    color: _selected == index
                        ? AppColors.primary
                        : Colors.grey[350])),
          ],
        ),
      ),
      onTap: () => setState(
        () {
          _selected = index;
        },
      ),
    );
  }
}
