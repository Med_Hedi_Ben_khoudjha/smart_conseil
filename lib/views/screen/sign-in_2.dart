import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/sign-in-1/login-widget.dart';
import 'package:smart_conseil/views/widget/sign-in-1/textWidget.dart';
import 'package:smart_conseil/views/widget/sign-in-2/button-widget.dart';
import 'package:smart_conseil/views/widget/sign-in-2/text-field-widget.dart';

class Sign_in_2 extends StatelessWidget {
  const Sign_in_2({super.key});
  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;

    return Scaffold(
            resizeToAvoidBottomInset: false,
        backgroundColor: AppColors.primary,


        body: SafeArea(
      child: Container(
        height: _deviceHeight,
        width: _devicewidth,
        color: AppColors.primary,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 30),
              child: Image(height: 120, image: AssetImage(AppAssets.logo)),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Welcom to 4In Shield!",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.w200),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Sign in to continue",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.w100),
                  ),
                ],
              ),
            ),
            TextFiledWidget(
                height: _deviceHeight,
                width: _devicewidth,
                hintText: "Username"),
            SizedBox(
              height: 15,
            ),
            TextFiledWidget(
                height: _deviceHeight,
                width: _devicewidth,
                hintText: "Password"),
            SizedBox(
              height: 20,
            ),
            BottonWidget(
              height: _deviceHeight,
              text: "Sign in",
              onPressed: () {
                Get.offNamed(Routes.space_chose);
              },
              width: _devicewidth * 0.3,
              haveIcon: true,
            ),
            SizedBox(
              height: 20,
            ),
            textWidget("Forgot password?")
          ],
        ),
      ),
    ));
  }
}
