import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/sign-in-2/button-widget.dart';
import 'package:smart_conseil/views/widget/sign-in-2/text-field-widget.dart';

class AddChild extends StatelessWidget {
  const AddChild({super.key});

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppColors.primary,
        body: SafeArea(
            child: Container(
          height: _deviceHeight,
          width: _devicewidth,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 0, vertical: 40),
                  height: _deviceHeight * 0.2,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.contain,
                          image: AssetImage(AppAssets.group3))),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                  width: _devicewidth * 0.8,
                  child: Text(
                    "Please enter the required information to complete your child’s profile",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
                TextFiledWidget(
                    height: _deviceHeight,
                    width: _devicewidth,
                    hintText: "First name"),
                SizedBox(
                  height: 15,
                ),
                TextFiledWidget(
                    height: _deviceHeight,
                    width: _devicewidth,
                    hintText: "Last name"),
                SizedBox(
                  height: 15,
                ),
                TextFiledWidget(
                    height: _deviceHeight,
                    width: _devicewidth,
                    hintText: "Gender"),
                SizedBox(
                  height: 15,
                ),
                TextFiledWidget(
                    height: _deviceHeight,
                    width: _devicewidth,
                    hintText: "Birth date"),
                SizedBox(
                  height: 15,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    BottonWidget(
                      height: _deviceHeight,
                      text: "Skip",
                      onPressed: () {
                        Get.back();
                      },
                      width: _devicewidth * 0.3,
                      haveIcon: false,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    BottonWidget(
                      height: _deviceHeight,
                      text: "Next",
                      onPressed: () {
                        Get.offNamed(Routes.congratsWizardEnd);
                      },
                      width: _devicewidth * 0.3,
                      haveIcon: true,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        )));
  }
}
