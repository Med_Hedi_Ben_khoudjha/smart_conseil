import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/child-dashboard-2/list-widget2.dart';

import 'package:smart_conseil/views/widget/child-dashboard/list-widget.dart';
import 'package:d_chart/d_chart.dart';

class ChildDashboard2 extends StatefulWidget {
  const ChildDashboard2({super.key});

  @override
  State<ChildDashboard2> createState() => _ChildDashboard2State();
}

class _ChildDashboard2State extends State<ChildDashboard2> {
  @override
  @override
  Widget build(BuildContext context) {
    final double _deviceHeight = MediaQuery.of(context).size.height;
    final double _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AppColors.primary,
        appBar: AppBar(
        elevation: 0,
        title: Text(
          "Child first name",
          style: TextStyle(fontSize: 25),
        ),
        backgroundColor: AppColors.primary,
        centerTitle: true,
        leading: Icon(
          Icons.menu,
          size: 35,
        ),
        actions: [
          Icon(
            Icons.person,
            size: 35,
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          height: _deviceHeight,
          width: _devicewidth,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Padding(
                    padding: EdgeInsets.all(16),
                    child: AspectRatio(
                      aspectRatio: 16 / 9,
                      child: Container(
                        width: _devicewidth * 0.8,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12)),
                        child: Column(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("Mental State",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: _deviceHeight,
                                width: _devicewidth * 0.8,
                                child: DChartBar(
                                  data: [
                                    {
                                      'id': 'Bar',
                                      'data': [
                                        {'domain': '2020', 'measure': 3},
                                        {'domain': '2021', 'measure': 4},
                                        {'domain': '2022', 'measure': 6},
                                        {'domain': '2023', 'measure': 0.3},
                                      ],
                                    },
                                  ],
                                  domainLabelPaddingToAxisLine: 16,
                                  axisLineTick: 2,
                                  axisLinePointTick: 2,
                                  axisLinePointWidth: 10,
                                  axisLineColor: AppColors.primary,
                                  measureLabelPaddingToAxisLine: 16,
                                  barColor: (barData, index, id) =>
                                      AppColors.primary,
                                  showBarValue: true,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
                SizedBox(
                  height: 15,
                ),
                Listwidget2(deviceHeight: _deviceHeight, devicewidth: _devicewidth),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
