import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/sign-in-2/button-widget.dart';
import 'package:smart_conseil/views/widget/sign-in-2/text-field-widget.dart';

class AddProfile extends StatelessWidget {
  const AddProfile({super.key});

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppColors.primary,
        body: SafeArea(
            child: Container(
          height: _deviceHeight,
          width: _devicewidth,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 40),
                height: _deviceHeight * 0.2,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.contain,
                        image: AssetImage(AppAssets.group))),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                width: _devicewidth * 0.8,
                child: Text(
                  "Please enter your child’s social media account information ",
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 3),
                width: _devicewidth * 0.8,
                child: Text(
                  "This information will help us to protect your child from online abusive content and cyberharassment ",
                  textAlign: TextAlign.center,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextFiledWidget(
                  height: _deviceHeight,
                  width: _devicewidth,
                  hintText: "Social network platform (Drop down)"),
              SizedBox(
                height: 15,
              ),
              TextFiledWidget(
                  height: _deviceHeight,
                  width: _devicewidth,
                  hintText: "Social network user name"),
              SizedBox(
                height: 15,
              ),
              TextFiledWidget(
                  height: _deviceHeight,
                  width: _devicewidth,
                  hintText: "Social network page URL"),
              SizedBox(
                height: 15,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BottonWidget(
                    height: _deviceHeight,
                    text: "Skip",
                    onPressed: () {
                      Get.back();
                    },
                    width: _devicewidth * 0.3,
                    haveIcon: false,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  BottonWidget(
                    height: _deviceHeight,
                    text: "Next",
                    onPressed: () {
                      Get.offNamed(Routes.confirmProfile);
                    },
                    width: _devicewidth * 0.3,
                    haveIcon: true,
                  ),
                ],
              )
            ],
          ),
        )));
  }
}
