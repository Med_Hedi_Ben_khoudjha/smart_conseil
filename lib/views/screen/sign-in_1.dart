import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/sign-in-1/login-widget.dart';
import 'package:smart_conseil/views/widget/sign-in-1/textWidget.dart';

class Sign_in_1 extends StatelessWidget {
  const Sign_in_1({super.key});
  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AppColors.primary,
        body: SafeArea(
      child: Container(
        height: _deviceHeight,
        width: _devicewidth,
        color: AppColors.primary,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 30),
              child: Image(height: 120, image: AssetImage(AppAssets.logo)),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Welcom to 4In Shield!",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.w200),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Sign in to continue",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.w100),
                  ),
                ],
              ),
            ),
            SignIn(_devicewidth, _deviceHeight, "Sign in with Email",
                AppAssets.email),
            SizedBox(
              height: 15,
            ),
            SignIn(_devicewidth, _deviceHeight, "Sign in with Gmail",
                AppAssets.google),
            SizedBox(
              height: 15,
            ),
            SignIn(_devicewidth, _deviceHeight, "Sign in with Facebook",
                AppAssets.facebook),
            SizedBox(
              height: 15,
            ),
            InkWell(
                onTap: (() {
                  Get.offNamed(Routes.sign_in_3);
                }),
                child: textWidget("No account? Sign up"))
          ],
        ),
      ),
    ));
  }
}
