import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';

class SpaceChose extends StatelessWidget {
  const SpaceChose({super.key});

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: AppColors.primary,
        body: SafeArea(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 40, vertical: 60),
                    width: _devicewidth * 0.8,
                    child: Text(
                      "Who is going to use this device?",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 30),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.offNamed(Routes.bienvenu);
                    },
                    child: Container(
                      height: _devicewidth * 0.4,
                      width: _devicewidth * 0.8,
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15)),
                      child: Text(
                        "Parents",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      Get.offNamed(Routes.bienvenu);
                    },
                    child: Container(
                      height: _devicewidth * 0.4,
                      width: _devicewidth * 0.8,
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15)),
                      child: Text(
                        "Child",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
              Positioned(
                bottom: _deviceHeight * 0.57,
                left: _devicewidth * 0.64,
                child: Image.asset(
                  AppAssets.standing6,
                  height: 100,
                ),
              ),
              Positioned(
                bottom: _deviceHeight * 0.4821,
                left: _devicewidth * 0.634,
                child: Image.asset(
                  AppAssets.sprint,
                  height: 90,
                ),
              ),
              Positioned(
                bottom: _deviceHeight * 0.19,
                left: _devicewidth * 0.64,
                child: Image.asset(
                  AppAssets.sprint2,
                  height: 90,
                  width: 100,
                ),
              ),
              Positioned(
                bottom: _deviceHeight * 0.25,
                left: _devicewidth * 0.634,
                child: Image.asset(
                  AppAssets.standing23,
                  height: 120,
                  width: 80,
                ),
              ),
            ],
          ),
        ));
  }
}
