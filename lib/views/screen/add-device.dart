import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';
import 'package:smart_conseil/views/widget/add-device/text-container-widget.dart';
import 'package:smart_conseil/views/widget/sign-in-2/button-widget.dart';

class AddDevice extends StatelessWidget {
  const AddDevice({super.key});

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppColors.primary,
        body: SafeArea(
            child: SizedBox(
          height: _deviceHeight,
          width: _devicewidth,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 60, 0, 15),
                width: _devicewidth * 0.8,
                child: Text(
                  "Please add your child's devices ",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 30),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                width: _devicewidth * 0.8,
                child: Text(
                  "The information from your child’s device will help us to assess their mental health, duration of sleep, places visited, used applications  etc. It also gives you the ability to block or allow installed applications ",
                  textAlign: TextAlign.justify,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
                width: _devicewidth * 0.8,
                child: Text(
                  "Please follow the following steps: ",
                  textAlign: TextAlign.justify,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
              textContainer(_devicewidth, "1. Bring your child’s mobile phone"),
              textContainer(_devicewidth,
                  "2. Scan this QR code with your child’s device camera"),
              textContainer(_devicewidth,
                  "3. You will be redirected to the 4IN shield application download page"),
              textContainer(_devicewidth,
                  "4. Once the application is installed, click Next "),
              Container(
                alignment: Alignment.center,
                width: double.infinity,
                height: _deviceHeight * 0.2,
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(AppAssets.frame))),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BottonWidget(
                    height: _deviceHeight,
                    text: "Skip",
                    onPressed: () {
                      Get.back();
                    },
                    width: _devicewidth * 0.3,
                    haveIcon: false,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  BottonWidget(
                    height: _deviceHeight,
                    text: "Next",
                    onPressed: () {
                      Get.offNamed(Routes.addProfile);
                    },
                    width: _devicewidth * 0.3,
                    haveIcon: true,
                  ),
                ],
              )
            ],
          ),
        )));
  }
}
