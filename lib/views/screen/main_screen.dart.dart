import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/logic/controllers/main-controller.dart';
import 'package:smart_conseil/utils/app-colors.dart';

class MainScreen extends StatelessWidget {
  MainScreen({super.key});
  final controller = Get.find<MainController>();

  @override
  Widget build(BuildContext context) {
    final _deviceHeight = MediaQuery.of(context).size.height;
    final _devicewidth = MediaQuery.of(context).size.width;

    return SafeArea(child: Obx(() {
      return Scaffold(
          backgroundColor: AppColors.primary,
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: controller.currentIndex.value,
            backgroundColor: Colors.grey,
            type: BottomNavigationBarType.fixed,
            selectedLabelStyle: TextStyle(fontSize: 15),
            unselectedLabelStyle: TextStyle(color: AppColors.secondary),
            unselectedItemColor: AppColors.secondary,
            selectedItemColor: Colors.white,
            showSelectedLabels: true,
            showUnselectedLabels: true,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.phone_android,
                    color: AppColors.secondary,
                  ),
                  label: "Dashbaord",
                  activeIcon: Icon(
                    Icons.phone_android,
                    color: Colors.white,
                  )),
              BottomNavigationBarItem(
                  label: "Phone usage",
                  icon: Icon(
                    Icons.phone_android,
                    color: AppColors.secondary,
                  ),
                  activeIcon: Icon(
                    Icons.phone_android,
                    color: Colors.white,
                  )),
              BottomNavigationBarItem(
                  label: "Social media",
                  icon: Icon(
                    Icons.phone_android,
                    color: AppColors.secondary,
                  ),
                  activeIcon: Icon(
                    Icons.phone_android,
                    color: Colors.white,
                  )),
              BottomNavigationBarItem(
                  label: "Alerts",
                  icon: Icon(
                    Icons.phone_android,
                    color: AppColors.secondary,
                  ),
                  activeIcon: Icon(
                    Icons.phone_android,
                    color: Colors.white,
                  )),
            ],
            onTap: (index) {
              controller.currentIndex.value = index;
            },
          ),
          body: Container(
            child: IndexedStack(
              index: controller.currentIndex.value,
              children: controller.tabs.value,
            ),
          ));
    }));
  }
}
