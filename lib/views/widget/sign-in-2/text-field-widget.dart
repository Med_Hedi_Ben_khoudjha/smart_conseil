import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class TextFiledWidget extends StatelessWidget {
  double height;
  double width;
  String hintText;
  TextFiledWidget({Key? Key, required this.height, required this.width,required this.hintText}):super(key: Key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width * 0.8,
        child: TextField(
          decoration: InputDecoration(
            fillColor: Colors.white,
            hintText: hintText,
            hintStyle: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: Colors.grey,
            ),
            filled: true,
            enabledBorder: OutlineInputBorder(
                // ignore: prefer_const_constructors
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(10)),
            focusedBorder: OutlineInputBorder(
                // ignore: prefer_const_constructors
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(10)),
            errorBorder: OutlineInputBorder(
                // ignore: prefer_const_constructors
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(10)),
            focusedErrorBorder: OutlineInputBorder(
                // ignore: prefer_const_constructors
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(10)),
          ),
        ));
  }
}
