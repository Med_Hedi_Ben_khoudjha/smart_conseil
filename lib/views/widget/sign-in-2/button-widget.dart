// ignore_for_file: sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-colors.dart';

class BottonWidget extends StatelessWidget {
  double height;
  double width;
  String text;
  bool haveIcon;
  Function() onPressed;
  BottonWidget(
      {Key? Key,
      required this.height,
      required this.width,
      required this.text,
      required this.onPressed,
      required this.haveIcon})
      : super(key: Key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: ElevatedButton(
        onPressed: onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              text,
              style: TextStyle(
                  color: AppColors.primary,
                  fontWeight: FontWeight.w100,
                  fontSize: 20),
            ),
            haveIcon
                ? Expanded(
                    child: Icon(
                      Icons.arrow_forward_ios_rounded,
                      color: AppColors.primary,
                      size: 20,
                    ),
                  )
                : Container(),
          ],
        ),
        style: ElevatedButton.styleFrom(
            primary: Colors.white, minimumSize: const Size(360, 50)),
      ),
    );
  }
}
