import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/utils/app-colors.dart';

class SearchFormText extends StatelessWidget {
  SearchFormText({super.key});
  TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: (value) {},
      controller: textEditingController,
      style: TextStyle(color: Colors.black),
      cursorColor: Colors.white,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppColors.primary), //<-- SEE HERE
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color:  AppColors.primary), //<-- SEE HERE
            borderRadius: BorderRadius.circular(10.0),
          ),
          errorBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppColors.primary), //<-- SEE HERE
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color:  AppColors.primary), //<-- SEE HERE
            borderRadius: BorderRadius.circular(10.0),
          ),
          fillColor: Color(0x00000000).withOpacity(0.1),
          focusColor: Colors.red,

          hintStyle: TextStyle(
              color: Get.isDarkMode ? Colors.grey : Colors.grey,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          filled: true,
          prefixIcon: Icon(
            Icons.search,
            color: Colors.white,
          )),
    );
  }
}
