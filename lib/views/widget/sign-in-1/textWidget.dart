import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget textWidget(String text) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
    alignment: Alignment.centerLeft,
    child: Text(
      text,
      style: TextStyle(
        fontSize: 20,
        decoration: TextDecoration.underline,
        color: Colors.white.withOpacity(0.8),
      ),
    ),
  );
}
