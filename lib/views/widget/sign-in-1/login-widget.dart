import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_conseil/routes/routes.dart';
import 'package:smart_conseil/utils/app-assets.dart';
import 'package:smart_conseil/utils/app-colors.dart';

Widget SignIn(
    double _devicewidth, double _deviceHeight, String text, String icon) {
  return InkWell(
    onTap: () {
      Get.toNamed(Routes.sign_in_2);
    },
    child: Container(
      width: _devicewidth * 0.8,
      height: _deviceHeight * 0.08,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12), color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                Text(
                  text,
                  style: TextStyle(fontSize: 18, color: Colors.blue.shade200),
                ),
                SizedBox(
                  width: 5,
                ),
                Image(height: 22, image: AssetImage(icon)),
              ],
            ),
          ),
          Container(
            child: Icon(
               Icons.arrow_forward_ios_rounded,
              color: AppColors.primary,
              size: 20,
            ),
          )
        ],
      ),
    ),
  );
}
