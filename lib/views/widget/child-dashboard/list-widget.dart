import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:smart_conseil/utils/app-colors.dart';

class Listwidget extends StatelessWidget {
  double deviceHeight;
  double devicewidth;
  Listwidget({Key? key, required this.deviceHeight, required this.devicewidth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        width: devicewidth * 0.9,
        height: deviceHeight * 0.3,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(12)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Last Alerts",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
            ),
            Expanded(
              child: RawScrollbar(
                thumbColor: AppColors.primary,

                radius: Radius.circular(20),

                child: ListView(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(8),
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    itemList("Toxic comment!", "12:34 PM",
                        "Mama always said life was like a box of chocolates. You never know what…"),
                    SizedBox(
                      height: 5,
                    ),
                    itemList("Toxic comment!", "12:34 PM",
                        "Mama always said life was like a box of chocolates. You never know what…"),
                    SizedBox(
                      height: 5,
                    ),
                    itemList("Sleep depreviation!", "12:34 PM",
                        "Mama always said life was like a box of chocolates. You never know what…"),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget itemList(String text, String text2, String text3) {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                text,
                style: TextStyle(color: Colors.red, fontSize: 17),
              ),
              Text(
                text2,
                style: TextStyle(color: Colors.grey, fontSize: 14),
              ),
            ],
          ),
          SizedBox(
            height: 7,
          ),
          Text(
            text3,
            style: TextStyle(fontSize: 17),
          ),
        ],
      ),
    );
  }
}
