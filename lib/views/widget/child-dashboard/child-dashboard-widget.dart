import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class ChildDashboardWidget extends StatelessWidget {
  double deviceHeight;
  double devicewidth;
  String title;
  String title2;
  String description;
  String description2;
  bool haveColor = false;
  ChildDashboardWidget(
      {Key? Key,
      required this.deviceHeight,
      required this.devicewidth,
      required this.title,
      required this.title2,
      required this.description,
      required this.haveColor,
      required this.description2})
      : super(key: Key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: deviceHeight * 0.15,
            width: devicewidth * 0.4,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  description,
                  style: TextStyle(
                      fontSize: 20,
                      color: haveColor ? Colors.red : Colors.grey),
                )
              ],
            ),
          ),
          SizedBox(width: 25,),
          Container(
            height: deviceHeight * 0.15,
            width: devicewidth * 0.4,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title2,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  description2,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
