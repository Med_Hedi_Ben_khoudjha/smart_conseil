import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget textContainer(double _devicewidth,String  text) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
    width: _devicewidth * 0.8,
    child: Text(
      text,
      textAlign: TextAlign.justify,
      textDirection: TextDirection.ltr,
      style: TextStyle(color: Colors.white, fontSize: 20),
    ),
  );
}
