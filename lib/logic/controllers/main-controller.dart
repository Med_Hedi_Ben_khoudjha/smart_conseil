import 'package:get/state_manager.dart';
import 'package:smart_conseil/views/screen/application-list.dart';
import 'package:smart_conseil/views/screen/child-dashboard.dart';
import 'package:smart_conseil/views/screen/child-dashboard2.dart';
import 'package:smart_conseil/views/screen/child-dashboard3.dart';




class MainController extends GetxController {
  RxInt currentIndex = 0.obs;

  final tabs =
      [ChildDashboard(), ChildDashboard2(), ChildDashboard3(), ApplicationList()].obs;
}
