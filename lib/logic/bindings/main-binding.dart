import 'package:get/get.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:smart_conseil/logic/controllers/main-controller.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(MainController());
  }
}
